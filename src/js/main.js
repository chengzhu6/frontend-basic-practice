async function getInfo(url) {
  try {
    const result = await fetch(url);
    const data = await result.json();
    // eslint-disable-next-line no-console
    initHtml(data);
  } catch (e) {
    // eslint-disable-next-line no-console
    console.log(e);
  }
}

function initHtml(data) {
  // eslint-disable-next-line no-console
  data.educations.map(e => {
    const year = e.year;
    const title = e.title;
    const description = e.description;
    // eslint-disable-next-line no-undef
    $('ul').append(`<li>
                    <span>${year}</span>
                    <section class="content">
                        <h2 class="title">${title}</h2>
                        <p class="description">${description}</p>
                    </section>
                </li>`);
  });
  const person = data;
  // eslint-disable-next-line no-undef
  $('article').append(
    `<p class="person-description">${person.description}</p>`
  );
  // eslint-disable-next-line no-undef
  $('header').append(
    `<h1>MY NAME IS ${person.name} ${person.year}YO AND THIS IS MY RESUME/CV</h1>`
  );
}
